# saporiPinocchios
Food cart company site w/ cart location map.  Meteor was used as a build tool and a database to store information on the carts' locations and make those locations available to users.

____

### Site Screenshots
![Landing section](/screenshot1.png?raw=true "Site Landing Section")
![Landing section](/screenshot2.png?raw=true "Site Landing Section")
