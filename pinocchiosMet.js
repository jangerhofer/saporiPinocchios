if (Meteor.isClient) {
  Meteor.startup(function() {
    GoogleMaps.load({
      libraries: 'places'
    })

    $(window).scroll(function() {
      var perc, windowPosition;
      windowPosition = $(window).scrollTop() + $(window).height();
      perc = windowPosition / $(document).height() * 100;
      if (perc > 25 && perc < 50) {
        Session.set("scrollPercentage", 25);
      }
      if (perc > 50 && perc < 75) {
        Session.set("scrollPercentage", 50);
      }
      if (perc > 75 && perc < 99) {
        Session.set("scrollPercentage", 75);
      }
      if (perc > 99) {
        return Session.set("scrollPercentage", 100);
      }
    });
  })

  // Analytics code
  Tracker.autorun(function() {
    sectionScroll = Session.get("scrollPercentage")
    analytics.track(sectionScroll + "View")
  })

  Template.mapTemp.helpers({
    find_closest_marker: function(event) {
      function rad(x) {
        return x * Math.PI / 180
      }
      var lat = event.latLng.lat()
      var lng = event.latLng.lng()
      var R = 6371 // radius of earth in km
      var distances = []
      var closest = -1
      markers = bikes.find().fetch()
      for (i = 0; i < markers.length; i++) {
        var mlat = markers[i].lat
        var mlng = markers[i].long
        var dLat = rad(mlat - lat)
        var dLong = rad(mlng - lng)
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
          Math.cos(rad(lat)) * Math.cos(rad(lat)) * Math.sin(dLong / 2) * Math.sin(dLong / 2)
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        var d = R * c
        distances[i] = d
        if (closest == -1 || d < distances[closest]) {
          closest = i
        }
      }
    },
    exampleMapOptions: function() {

      var styles = [{
        stylers: [{
          hue: "#68CBEC"
        }, {
          saturation: 100
        }]
      }, {
        featureType: "road",
        elementType: "geometry",
        stylers: [{
          lightness: 100
        }, {
          visibility: "simplified"
        }]
      }, {
        featureType: "road",
        elementType: "labels",
        stylers: [{
          visibility: "off"
        }]
      }]


      // Make sure the maps API has loaded
      if (GoogleMaps.loaded()) {
        // Map initialization options
        return {
          center: new google.maps.LatLng(42.3389, -71.090127),
          zoom: 15,
          scrollwheel: false,
          styles: styles
        }
      }
    }
  })

  Template.mapTemp.events({

  })

  Template.mapTemp.rendered = function() {
    GoogleMaps.ready('exampleMap', function(map) {

      $("#addressSearch").geocomplete();

      infowindow = new google.maps.InfoWindow({
        content: "A bike!",
        zIndex: 100
      })

      /*
      var marker = new google.maps.Marker({
        position: map.options.center,
        map: map.instance
      })
      */
      var image = {
        url: '/images/bicycleIcon.png',
        // This marker is 20 pixels wide by 32 pixels high.
        size: new google.maps.Size(80, 80),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(0, 0)
      }

      liveMarkers = LiveMaps.addMarkersToMap(
        map.instance, [{
          cursor: bikes.find(),
          onClick: function(marker, other) {
            //infowindow.open(map.instance, marker)
            alert(JSON.stringify(marker), other)
          },
          transform: function(document) {
            return {
              title: document.driver,
              position: {
                lat: document.lat,
                lng: document.long
              },
              icon: image,
              data: {
                id: document._id
              }
            }
          }
        }]
      )

    })

  }
}

// GoogleMaps.maps.exampleMap.instance.event.addListener('exampleMap', 'click', find_closest_marker)
